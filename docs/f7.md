---
id: f7
title: Les Permissions
---

La sécurité est un sujet compliqué. D'un coté, tout le monde la désire, et de l'autre, personne ne veut s'en occuper. La stratégie 
des systèmes d'exploitation mobiles est de contrôler le plus strictement possible l'accès aux ressources de l'appareil, ce qui
réduit grandement le nombre de vecteurs d'attaque. Chaque application désirant accéder à une ressource doit donc en faire 
explicitement la demande. C'est ce que l'on appelel une [permission](https://developer.android.com/guide/topics/permissions/overview).

Dans cette section, nous verrons à quoi servent les permissions et comment les obtenir.

## *Principle of least privilege*

Le concept appliqué par les systèmes d'exploitation mobiles est le
[*Principle of least privilege*](https://en.wikipedia.org/wiki/Principle_of_least_privilege). Appliquer ce principe signifie que
les applications n'ont initialement aucun droit et doivent alors demander l'accès à toutes les ressources dont elles ont besoin. 

Par exemple, si une application désire accéder à la caméra, elle doit en faire la demande au système d'exploitation qui, à son tour, 
en fera la demande à l'utilisateur. Si elle tente d'accéder à une ressource dont l'accès ne lui est pas accordé, elle sera automatiquement 
arrêtée par le système avant de faire des dégats.

## Un mauvais design

![Droits demandés à l'installation vs Droits demandés à l'exécution](assets/img/f7/rights.png) <span />
Jusqu'à Android 5.1.1, les permissions étaient accordées aux applications lors de l'installation (*voir capture d'écran ci-contre*) [^1]. 
Cependant, il a été rapidement constaté que les utilisateurs accordaient ces permissions sans les lire. 

Afin de comprendre pourquoi, regardez tout simplement cette vidéo.

<div class="center">
    <video controls>
      <source src="/mobile1-doc/docs/assets/video/f7/common_user.mp4" type="video/mp4">
    </video>
</div>

De nos jours, considérer que l'utilisateur moyen a le quotient intellectuel d'une roche n'est pas une exagération. **Le maillon 
faible, quand il est question de sécurité, est l'utilisateur lui-même.** 

À la place, depuis Android 6.0, les application doivent demander les permissions au moment où elles sont nécessaires. Cela permet
de ne pas surcharger mentalement l'utilisateur au moment d'accorder les droits.

Cette expérience démontre à quel point la sécurité est une affaire de tous. La logique applicative, autant que le design
de l'interface, sont importants dans la création d'une application sécuritaire pour son utilisateur.

## Types de permissions

La plateforme *Android* sépare les permissions en deux catégories&nbsp;: les permissions dites
[normales](https://developer.android.com/guide/topics/permissions/overview#normal_permissions)
et les permissions dites [dangeureuses](https://developer.android.com/guide/topics/permissions/overview#permission-groups).

### Permissions Normales

Les permissions [normales](https://developer.android.com/guide/topics/permissions/overview#normal_permissions) sont celles qui ne 
peuvent pas compromettre la confidentialité des données de l'utilisateur. Par exemple, un application malveillante qui désactive le 
*Wifi* pourrait être désagréable, mais cela ne fera jamais rien au données confidentielles de l'utilisateur. Ces permissions sont donc 
automatiquement accordées par le système lorsqu'elles sont demandées.

### Permissions Dangeureuses

À l'inverse, les permissions dangeureuses donnent un accès direct aux données confidentielles d'un utilisateur, tel que sa position
GPS, ses contacts ou même ses fichiers. Ces permissions doivent donc être demandées à l'utilisateur directement en passant
par le système d'exploitation.

## Le manifeste et les permissions

Le fichier `AndroidManifest.xml` contient non seulement la liste des activités d'une application, mais aussi la liste
des permissions qu'elle requiert, qu'elles soient dangereuses ou non.

```xml
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="ca.csf.mobile1.demo">

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.CAMERA" />

    <!-- Reste du fichier omis -->

</manifest>
```

Dans l'exemple ci-dessus, l'application demande deux permissions : une normale (`INTERNET`) et une dangereuse (`CAMERA`). La permission
`INTERNET` sera automatiquement accordée, tandis que la permission `CAMERA` devra être accordée par l'utilisateur.

## Demander une permission

[Demander une permission dangereuse](https://developer.android.com/training/permissions/requesting.html) est plus simple que l'on 
pourrait le croire, car cela suit toujours la même logique. La première étape est de vérifier si la permission est déjà accordée. 
Si ce n'est pas le cas, il faut vérifier si l'utilisateur avait déjà refusé d'accorder cette permission. Dans l'affirmative, mieux vaut
expliquer à l'usager pourquoi l'application requiert la permission.

Demander une permission ressemble donc à ceci :

```java
public class TakePictureActivity extends AppCompatActivity {
    
    private static final int CAMERA_PERMISSION_REQUEST = 1;
    
    private void askForCameraPermission() {
        //La permission est-elle acordée ?
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) 
            != PackageManager.PERMISSION_GRANTED) {
            //La permission n'est pas accordée
            
            //Faut-il l'expliquer à l'utilisateur ?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                //Afficher une explication.
            } else {
                ActivityCompat.requestPermissions(this,
                                                  new String[]{Manifest.permission.CAMERA},
                                                  CAMERA_PERMISSION_REQUEST);
            }            
        }
    }
    
    @Override
    public void onRequestPermissionsResult(int requestCode, 
                                           String permissions[], 
                                           int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST: {
                //Vide si l'utilisateur a cliqué sur "Annuler"
                if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permission accordée!
                } else {
                    //Permission refusée. Expliquer à l'utilisateur pourquoi c'est important.
                }
                return;
            }
        }
    }
    
}
```

Adaptez ce code à vos propres besoin. Notez que vous pouvez aussi demander plusieurs permissions en même temps.

[^1]: Google (2019), 
[Install-time requests](https://developer.android.com/guide/topics/permissions/overview#install-time_requests_android_511_and_below), 
*Google*



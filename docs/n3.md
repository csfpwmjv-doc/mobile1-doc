---
id: n3
title: Mise en forme
---

La mise en forme aide énormément à la lisibilité du code. Elle doit être au centre des préoccupations du programmeur. Aussi, compte
tenu que les *IDE* modernes peuvent tous formater le code, il n’y a aucune excuse pour que ce dernier ne le soit pas.

## Indentation, accolades « { » et « } » et longueur des lignes
Utilisez 4 espaces et non pas une tabulation pour indenter. Indentez d'un niveau à l'intérieur 
d'accolades `{` et `}`. Les accolades `{` débutent sur la même ligne. Les accolades `}` sont toujours 
sur une nouvelle ligne. Vous pouvez ne pas utiliser d'accolades après une condition ou une boucle si 
l'instruction est très courte. Sinon, toujours utiliser des accolades, même si le langage considère 
cela facultatif.

Une ligne de code ne doit jamais dépasser 160 caractères. Aussi, si possible, remplacez les conditions
produisant des booléens par des affectations.

<blockquote class="good">

**Correct**

```java
if (healthPoints > 100) {
    EnnemyFactory.create().isBoss(true).addBehaviour(new SplitBehaviour(40));
}
```

```java
if (healthPoints <= 0) isDead = true;
```

```java
isDead = healthPoints <= 0;
```

</blockquote>

<blockquote class="bad">

**Incorrect**

```java
if (healthPoints <= 0) 
{
    isDead = true;
}
```

```java
if (healthPoints <= 0) 
{
isDead = true;
}
```

```java
if (healthPoints > 100) EnnemyFactory.create().isBoss(true).addBehaviour(new SplitBehaviour(40));
```

```java
if (healthPoints <= 0)
isDead = true;
```

</blockquote>

## Espacement vertical et regroupements (Retours de chariot)
Regroupez :
 * les variables/constantes/attributs fortement liées.
 * les lignes de code fortement liées.

Séparez chaque regroupement d'une ligne vide.

<blockquote class="good">

**Correct**

```java
private Texture visual;
private float minSize;
private float maxSize;

private float minExplosionForce;
private float maxExplosionForce;
```

</blockquote>

<blockquote class="bad">

**Incorrect**

```java
private Texture visual;
private float maxSize;
private float minExplosionForce;
private float minSize;
private float maxExplosionForce;
```

</blockquote>

## Ordre des éléments dans une classe ou une structure
Les éléments à l'intérieur d'une classe doivent être placés dans cet ordre :
 1. **Constantes** - *Public, suivi de privé*
 2. **Attributs statiques** - *Privé uniquement*
 3. **Méthodes statiques** - *Public et privé mélangé*
 4. **Attributs** - *Privé uniquement*
 5. **Constructeur par défaut** - *Public, privé ou protégé*
 6. **Constructeurs avec paramètres** - *Public, privé et protégé mélangé*
 7. **Méthodes** - *Public, privé et protégé mélangé*
 8. **Énumérations internes** - *Public, suivi de privé et de protégé*
 9. **Classes internes**  - *Public, suivi de privé et de protégé*
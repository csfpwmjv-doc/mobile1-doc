//See https://docusaurus.io/docs/site-config for all the possible site configuration options.
const siteConfig = {
    title: 'Programmation Mobile I',
    tagline: 'Portail du cours Programmation Mobile I',
    url: 'https://csfpwmjv.gitlab.io/',
    baseUrl: '/mobile1-doc/',
    projectName: 'site',
    organizationName: 'Cégep de Sainte-Foy',
    headerLinks: [
        {doc: 'f1', label: 'Notes de cours'},
    ],
    headerIcon: 'img/android.svg',
    footerIcon: 'img/android.svg',
    favicon: 'img/favicon.ico',
    colors: {
        primaryColor: '#394053', //Header navigation bar and sidebars.
        secondaryColor: '#262b39', //Second row of navigation bar when on mobile.
    },
    copyright: `Copyright © ${new Date().getFullYear()} Benjamin Lemelin`,
    highlight: {
        theme: 'androidstudio' //Highlight.js theme
    },
    markdownPlugins: [
        function externalLinksIntoNewTab(md) {
            md.renderer.rules.link_open = (function () {
                let original = md.renderer.rules.link_open;
                return function () {
                    let link = original.apply(this, arguments);
                    if (link.includes("http"))
                        return link.substring(0, link.length - 1) + ' target="_blank" rel="noreferrer noopener">';
                    else
                        return link
                };
            })();
        },
    ],
    scripts: [], //Custom scripts here that would be placed in <script> tags.
    stylesheets: [], //Custom stylesheets.
    onPageNav: 'separate', //Show right navigation on a documentation page.
    cleanUrl: true, //Allow no .html extensions for paths.
};

module.exports = siteConfig;

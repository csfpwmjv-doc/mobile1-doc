#!/bin/bash
set -e

rm -rf "public"
mkdir "public"
cp -R "build/." "public"
